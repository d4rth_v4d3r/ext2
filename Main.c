/*
 * Main.c
 *
 *  Created on: 15/06/2013
 *      Author: julian
 */
#include "String.h"
#include "My_Ext2.h"

int readPath(char *args) {
	fscanf(stdin, "%s", args);
	if (*(args) != '/') {
		printf("ERROR**: La direccion %s no es absoluta\n", args);
		return -1;
	}
	return 0;
}

int fexists(char *ruta) {
	if (fopen(ruta, "r") == NULL ) {
		printf("La direccion %s no existe\n", ruta);
		return -1;
	}
	return 0;
}

void parseFile(EXT2 *ext2, char *path) {
	FILE * input = fopen(path, "r");
	if (input == NULL ) {
		printf("No se pudo abrir el archivo(¿existe?).\n");
		return;
	}
	char read;
	int flag = NULO;
	int i = 0;
	char *id = malloc(15 * sizeof(char));
	char *ruta = malloc(100 * sizeof(char));
	char *buffer = malloc(128 * 128 * sizeof(char));
	char *contenido = malloc(22 * 128 * sizeof(char));
	reset(id, 15);
	reset(ruta, 100);
	reset(buffer, 128 * 128);
	reset(contenido, 22 * 128);

	while (1) {
		read = fgetc(input);
		if ((flag == File) && read == '"') {
			*(buffer + i) = read;
			i++;
			while ((read = fgetc(input)) != '"') {
				*(buffer + i) = read;
				++i;
			}
			*(buffer + i) = read;
		}
		if (((read) != '\r' && (read) != '\f' && (read) != '\n')) {
			*(buffer + i) = read;
			++i;
		} else {
			i = 0;
			if (sscanf(buffer, "%[^,],%s", id, ruta) == 2 && flag == Dir) {
				int r_size = len(ruta);
				if (r_size > 1) {
					char c = *(r_size + ruta);
					*(r_size + ruta) = *(r_size + ruta - 1) == '/' ? c : '/';
					r_size = c == '/' ? r_size : r_size + 1;
				}
				append(ruta, r_size, id);
				_mkdir(ext2, ruta);
			}
			if (sscanf(buffer, "%[^,], \"%[^\"]\", %s", id, contenido, ruta)
					== 3) {
				int r_size = len(ruta);
				if (r_size > 1) {
					char c = *(r_size + ruta);
					*(r_size + ruta) = *(r_size + ruta - 1) == '/' ? c : '/';
					r_size = c == '/' ? r_size : r_size + 1;
				}
				append(ruta, r_size, id);
				_mkfile(ext2, ruta, contenido);
			} else if (compare(id, "Directorios") == 0) {
				flag = Dir;
			} else if (compare(id, "Archivos") == 0) {
				flag = File;
			}
			reset(id, 15);
			reset(ruta, 100);
			reset(buffer, 128 * 128);
			reset(contenido, 22 * 128);
		}

		if (feof(input))
			break;
	}

	if (id != NULL )
		free(id);
	if (ruta != NULL )
		free(ruta);
	if (contenido != NULL )
		free(contenido);
	if (buffer != NULL )
		free(buffer);
	fclose(input);
}

void displayMenu() {
	char *instruccion = malloc(20 * sizeof(char));
	char *args = malloc(100 * sizeof(char));
	char *dName = malloc(100 * sizeof(char));
	char *buffer = malloc(128 * 128 * sizeof(char));
	char *fecha = malloc(20 * sizeof(fecha));
	time_t *h_actual = malloc(sizeof(time_t));
	reset(buffer, 128 * 128);
	reset(dName, 100);
	EXT2 *disk = NULL;

	do {
		if (disk != NULL )
			printf(">> %s ", disk->ruta);
		printf("~$  ");

		reset(args, 100);
		reset(instruccion, 20);

		//Leemos la instruccion
		fscanf(stdin, "%s", instruccion);

		if (compare(instruccion, "cdisk") == 0) {
			//Leemos la ruta del disco a crear
			if (readPath(args) == 0) {
				crearEXT2(args);
			}
		} else if (compare(instruccion, "mount") == 0) {
			//Leemos la ruta del disco a montar
			if (disk == NULL ) {
				if (readPath(dName) == 0) {
					disk = montarEXT2(dName);
					if (disk == NULL )
						printf("No se pudo montar el disco\n");
				}
			} else
				printf("Desmonte el disco primero\n");
		} else if (compare(instruccion, "umount") == 0) {
			//Leemos la ruta del disco a desmontar
			if (disk != NULL ) {
				desmontarEXT2(disk);
				disk = NULL;
				reset(dName, 100);
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "mkfile") == 0) {
			//Leemos la ruta del archivo y en que disco se va a crear
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					_mkfile(disk, args, "");
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "svfile") == 0) {
			//Leemos la ruta del archivo y en que disco se va a crear
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					if (_rmRuta(disk, args, 0) != -1) {
						fscanf(stdin, " %[^·]", buffer);
						_mkfile(disk, args, buffer);
						reset(buffer, 128 * 128);
						char *fecha = malloc(20 * sizeof(fecha));
						time_t *h_actual = malloc(sizeof(time_t));
						setFecha(h_actual);
						getFecha(*h_actual, fecha);
						setFecha(&disk->superbloque->fmodificacion);
						fprintf(disk->log, "%s:Archivo %s editado.\n", fecha,
								args);
						fflush(disk->log);
						free(fecha);
						free(h_actual);
					}
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "shfile") == 0) {
			//Leemos la ruta del archivo y en que disco esta para abrir
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					_opfile(disk, args);
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "rm") == 0) {
			//Leemos la ruta del archivo y en que disco esta para borrar
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					_rmRuta(disk, args, 1);
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "mkdir") == 0) {
			//Leemos la ruta de la carpeta y en que disco se va a crear
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					_mkdir(disk, args);
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "mkrep") == 0) {
			//Leemos la ruta de la carpeta y en que disco se va a borrar
			if (disk != NULL ) {
				int size = len(disk->ruta);
				*(disk->ruta + size) = '.';
				*(disk->ruta + size + 1) = 's';
				*(disk->ruta + size + 2) = 'u';
				*(disk->ruta + size + 3) = 'p';
				imprimirSuperBloque(disk->ruta, disk->superbloque);
				*(disk->ruta + size + 1) = 'b';
				*(disk->ruta + size + 2) = 'i';
				*(disk->ruta + size + 3) = 't';
				imprimirBitmap(disk->ruta, disk->bitmap);
				*(disk->ruta + size + 1) = 'b';
				*(disk->ruta + size + 2) = 'l';
				*(disk->ruta + size + 3) = 'o';
				imprimirBloques(disk, disk->ruta);
				*(disk->ruta + size) = '\0';
				*(disk->ruta + size + 1) = '\0';
				*(disk->ruta + size + 2) = '\0';
				*(disk->ruta + size + 3) = '\0';
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "loadf") == 0) {
			//Leemos la ruta de la carpeta y en que disco se va a crear
			if (disk != NULL ) {
				if (readPath(args) == 0) {
					parseFile(disk, args);
				}
			} else
				printf("No hay disco montado\n");
		} else if (compare(instruccion, "exit") == 0) {
			printf("Saliendo...\n");
			if (disk != NULL ) {
				desmontarEXT2(disk);
				disk = NULL;
			} else
				printf("No hay disco montado\n");
			break;
		} else {
			printf("Instruccion no valida\n");
		}
		//Descartamos el buffer
		scanf("%*[^\n]");
		scanf("%*c");
	} while (1);

	free(args);
	free(instruccion);
	free(buffer);
	free(dName);
	free(fecha);
	free(h_actual);
}

int main() {
	displayMenu();
	//menu();
	return 0;
}

