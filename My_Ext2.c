/*
 * My_Ext2.c
 *
 *  Created on: 18/06/2013
 *      Author: julian
 */
#include "My_Ext2.h"
#include "String.h"

/************************************************
 * 												*
 *	 	INICIO DE FUNCIONES DE CREACION			*
 * 												*
 ************************************************/

void setFecha(time_t *fecha) {
	time_t tiempo = time(0);
	*fecha = tiempo;
}

void getFecha(time_t tiempo, char *fecha) {
	struct tm *tlocal = localtime(&tiempo);
	strftime(fecha, 128, "%d/%m/%y_%H:%M", tlocal);
}

int crearEXT2(char *ruta) {
	///---------INTENTAMOS ABRIR EL ARCHIVO-----------
	FILE *disk = fopen(ruta, "wb");
	if (disk == NULL )
		return -1;

	///---------CREAMOS EL SUPERBLOQUE-----------
	SuperBloque *sb = malloc(sizeof(SuperBloque));
	sb->no_bloques = NO_BLOQUES;
	sb->no_inodos = NO_INODOS;
	sb->no_bloquesl = NO_BLOQUES;
	sb->no_inodosl = NO_INODOS;
	sb->pbloquel = 0;
	sb->pinodol = 0;
	sb->ap_pbloque = sizeof(SuperBloque) + sizeof(Bitmap)
			+ NO_INODOS * sizeof(Inodo);
	setFecha(&sb->fcreacion);
	setFecha(&sb->fmodificacion);
	setFecha(&sb->fmount);
	fwrite(sb, sizeof(SuperBloque), 1, disk);

	///---------CREAMOS LOS BITMAPS-----------
	Bitmap *bm = malloc(sizeof(Bitmap));
	crearMapabits(bm->bloques, NO_BLOQUES);
	crearMapabits(bm->inodos, NO_INODOS);
	fwrite(bm, sizeof(Bitmap), 1, disk);

	//---------CREAMOS LA TABLA DE INODOS-------
	Inodo *base = malloc(sizeof(Inodo));
	base->fsize = 0;
	base->no_bloques_asig = 0;
	base->ap_directo1 = -1;
	base->ap_directo2 = -1;
	base->ap_indirecto = -1;
	base->tipo = NULO;

	int i;
	for (i = 0; i < NO_INODOS; i++)
		fwrite(base, sizeof(Inodo), 1, disk);

	//---------CREAMOS TODOS LOS BLOQUES----------
	Bloque *bloque = malloc(sizeof(Bloque));
	for (i = 0; i < 128; i++)
		bloque->contenido[i] = '\0';
	for (i = 0; i < NO_BLOQUES; i++)
		fwrite(bloque, sizeof(Bloque), 1, disk);

	//---------LIBERAMOS LA MEMORIA----------
	free(sb);
	free(bm);
	free(base);
	free(bloque);

	//---------CERRAMOS EL ARCHIVO---------
	if (fclose(disk) != 0)
		return -1;

	EXT2 *new = montarEXT2(ruta);
	exec_mkdir(new, "/", NULL, -1, Dir);
	desmontarEXT2(new);

	int size = len(ruta);

	*(ruta + size) = '.';
	*(ruta + size + 1) = 'l';
	*(ruta + size + 2) = 'o';
	*(ruta + size + 3) = 'g';
	FILE *log = fopen(ruta, "w");
	*(ruta + size) = '\0';
	*(ruta + size + 1) = '\0';
	*(ruta + size + 2) = '\0';
	*(ruta + size + 3) = '\0';
	fclose(log);

	return 0;
}

EXT2 *montarEXT2(char *ruta) {
	FILE *disk = fopen(ruta, "r+b");
	if (disk == NULL )
		return NULL ;
	int size = len(ruta);
	*(ruta + size) = '.';
	*(ruta + size + 1) = 'l';
	*(ruta + size + 2) = 'o';
	*(ruta + size + 3) = 'g';
	FILE *log = fopen(ruta, "a");
	*(ruta + size) = '\0';
	*(ruta + size + 1) = '\0';
	*(ruta + size + 2) = '\0';
	*(ruta + size + 3) = '\0';
	EXT2 *ext2 = malloc(sizeof(EXT2));
	SuperBloque *sb = malloc(sizeof(SuperBloque));
	Bitmap *bm = malloc(sizeof(Bitmap));
	fread(sb, sizeof(SuperBloque), 1, disk);
	fseek(disk, sizeof(SuperBloque), SEEK_SET);
	fread(bm, sizeof(Bitmap), 1, disk);

	ext2->bitmap = bm;
	ext2->superbloque = sb;
	ext2->disco = disk;
	ext2->ruta = ruta;
	ext2->log = log;

	setFecha(&ext2->superbloque->fmount);
	escribirSuperBloque(ext2);

	return ext2;
}

void desmontarEXT2(EXT2 *ext2) {
	escribirBitmap(ext2);
	escribirSuperBloque(ext2);
	fclose(ext2->disco);
	fclose(ext2->log);
}

void crearMapabits(char *mapabits, int size) {
	int i;
	for (i = 0; i < size; i++)
		*(mapabits + i) = '0';
}

/************************************************
 * 												*
 *	 		INICIO DE FUNCIONES DE I/O			*
 * 												*
 ************************************************/

void escribirSuperBloque(EXT2 *ext2) {
	fseek(ext2->disco, 0, SEEK_SET);
	fwrite(ext2->superbloque, sizeof(SuperBloque), 1, ext2->disco);
}

void escribirBitmap(EXT2 *ext2) {
	fseek(ext2->disco, sizeof(SuperBloque), SEEK_SET);
	fwrite(ext2->bitmap, sizeof(Bitmap), 1, ext2->disco);
}

void escribirBloque(EXT2 *ext2, Bloque *b, int index) {
	fseek(ext2->disco, ext2->superbloque->ap_pbloque + index * sizeof(Bloque),
			SEEK_SET);
	fwrite(b, sizeof(Bloque), 1, ext2->disco);
	setFecha(&ext2->superbloque->fmodificacion);
}

void leerBloque(EXT2 *ext2, Bloque *b, int index) {
	fseek(ext2->disco, ext2->superbloque->ap_pbloque + index * sizeof(Bloque),
			SEEK_SET);
	fread(b, sizeof(Bloque), 1, ext2->disco);
}

void leerInodo(EXT2 *ext2, Inodo *i, int index) {
	fseek(ext2->disco,
			sizeof(SuperBloque) + sizeof(Bitmap) + index * sizeof(Inodo),
			SEEK_SET);
	fread(i, sizeof(Inodo), 1, ext2->disco);
}

void escribirInodo(EXT2 *ext2, Inodo *i, int index) {
	fseek(ext2->disco,
			sizeof(SuperBloque) + sizeof(Bitmap) + index * sizeof(Inodo),
			SEEK_SET);
	fwrite(i, sizeof(Inodo), 1, ext2->disco);
	setFecha(&ext2->superbloque->fmodificacion);
}

/************************************************
 * 												*
 *		INICIO DE FUNCIONES DE MANEJO			*
 * 												*
 ************************************************/

int obtenerBitLibre(char *bm, int size) {
	int i;

	for (i = 0; i < size; i++)
		if (*(bm + i) == '0')
			return i;
		else
			;
	return -1;
}

int agregarPuntero(Bloque *contenedor, int puntero) {
	int tpunteros = ASCIIToInteger(contenedor->contenido, 5);
	if (tpunteros >= 20)
		return -1;
	int i = 6;
	while (*(contenedor->contenido + i) != ' ')
		i++;

	IntegertoASCII(contenedor->contenido + i, puntero, 5);
	IntegertoASCII(contenedor->contenido, tpunteros + 1, 5);
	return puntero;
}

int agregarPunteroPunteros(EXT2 *ext2, Bloque *contenedor) {
	int tpunteros = ASCIIToInteger(contenedor->contenido, 5);
	if (tpunteros >= 20)
		return -1;
	int i = 6, cuenta = 0;

	Bloque *contenedor2 = malloc(sizeof(Bloque));
	while (cuenta < tpunteros && i < 128) {
		if (*(contenedor->contenido + i) != ' ') {
			int dirPointer = ASCIIToInteger(contenedor->contenido + i, 5);
			leerBloque(ext2, contenedor2, dirPointer);
			int tpunteros2 = ASCIIToInteger(contenedor2->contenido, 5);
			if (tpunteros2 >= 20) {
				cuenta++;
			} else {
				free(contenedor2);
				return dirPointer;
			}
		}
		i += 6;
	}

	*(ext2->bitmap->bloques + ext2->superbloque->pbloquel) = '1'; //Indicamos que vamos a ocupar ese bloque
	ext2->superbloque->no_bloquesl--;
	crearBloquePunteros(contenedor2); //Formateamos el bloque;
	escribirBloque(ext2, contenedor2, ext2->superbloque->pbloquel); //Escribimos en el indice del bitmap
	int bloquePunteros = ext2->superbloque->pbloquel;
	agregarPuntero(contenedor, bloquePunteros);
	ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
			NO_BLOQUES);
	free(contenedor2);
	contenedor2 = NULL;
	return bloquePunteros;
}

int borrarPuntero(Bloque *contenedor, int index) {
	int i, j;
	for (i = 6; i < 128; i += 6) {
		j = ASCIIToInteger(contenedor->contenido + i, 5);
		if (j == index) {
			*(contenedor->contenido + i) = ' ';
			*(contenedor->contenido + i + 1) = ' ';
			*(contenedor->contenido + i + 2) = ' ';
			*(contenedor->contenido + i + 3) = ' ';
			*(contenedor->contenido + i + 4) = ' ';

			int t = ASCIIToInteger(contenedor->contenido, 5);
			IntegertoASCII(contenedor->contenido, t - 1, 5);
			return j;
		}
	}

	return -1;
}

void crearBloquePunteros(Bloque *b) {
	IntegertoASCII(b->contenido, 0, 5);
	*(b->contenido + 5) = '|';
	int i;
	for (i = 6; i < sizeof(Bloque); i += 6) {
		*(b->contenido + i) = ' ';
		*(b->contenido + i + 1) = ' ';
		*(b->contenido + i + 2) = ' ';
		*(b->contenido + i + 3) = ' ';
		*(b->contenido + i + 4) = ' ';
		*(b->contenido + i + 5) = '|';
	}
}

void crearApuntadorDirecto1(EXT2* ext2, Inodo* raiz, Bloque *contenedor) {
	*(ext2->bitmap->bloques + ext2->superbloque->pbloquel) = '1'; //Indicamos que vamos a ocupar ese bloque
	ext2->superbloque->no_bloquesl--;
	escribirBloque(ext2, contenedor, ext2->superbloque->pbloquel); //Escribimos en el indice del bitmap
	raiz->ap_directo1 = ext2->superbloque->pbloquel; //Lo enlazamos
	ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
			NO_BLOQUES);
}

void crearApuntadorDirecto2(EXT2* ext2, Inodo* raiz, Bloque *contenedor) {
	*(ext2->bitmap->bloques + ext2->superbloque->pbloquel) = '1'; //Indicamos que vamos a ocupar ese bloque
	ext2->superbloque->no_bloquesl--;
	escribirBloque(ext2, contenedor, ext2->superbloque->pbloquel); //Escribimos en el indice del bitmap
	raiz->ap_directo2 = ext2->superbloque->pbloquel; //Lo enlazamos
	ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
			NO_BLOQUES);

}

void crearApuntadorIndirecto(EXT2* ext2, Inodo* raiz, Bloque *contenedor) {
	*(ext2->bitmap->bloques + ext2->superbloque->pbloquel) = '1'; //Indicamos que vamos a ocupar ese bloque
	ext2->superbloque->no_bloquesl--;
	crearBloquePunteros(contenedor); //Formateamos el bloque;
	escribirBloque(ext2, contenedor, ext2->superbloque->pbloquel); //Escribimos en el indice del bitmap
	raiz->ap_indirecto = ext2->superbloque->pbloquel; //Lo enlazamos
	ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
			NO_BLOQUES);
}

int exec_mkdir(EXT2 *ext2, char *id, Inodo *raiz, int index_raiz, int type) {
	if (ext2->superbloque->pinodol >= 0) { //Hay un inodo disponible
		Inodo *hijo = malloc(sizeof(Inodo));
		*(ext2->bitmap->inodos + ext2->superbloque->pinodol) = '1'; //Indicamos que vamos a ocupar ese bloque
		ext2->superbloque->no_inodosl--;
		int dir = -1;

		Bloque *contenedor = malloc(sizeof(Bloque));
		copy(hijo->id, id);
		hijo->fsize = 0;
		hijo->no_bloques_asig = 0;
		hijo->ap_directo1 = -1;
		hijo->ap_directo2 = -1;
		hijo->ap_indirecto = -1;
		hijo->tipo = type;

		if (index_raiz != -1 && raiz->ap_directo1 == -1) { //Si el nodo raiz no tiene apuntador directo, entonces lo creamos
			crearBloquePunteros(contenedor);
			crearApuntadorDirecto1(ext2, raiz, contenedor);
		}

		if (index_raiz != -1) {
			if (buscarRuta(ext2, raiz, id) != -1 && type == Dir) {
				printf("'%s' ya existe en '%s'\n", id, raiz->id);
				{
					if (hijo != NULL )
						free(hijo);
					hijo = NULL;
				}
				{
					if (contenedor != NULL )
						free(contenedor);
					contenedor = NULL;
				}
				return -1;
			}
			leerBloque(ext2, contenedor, raiz->ap_directo1);
			dir = agregarPuntero(contenedor, ext2->superbloque->pinodol);
			if (dir == -1) {
				if (raiz->ap_directo2 == -1) { //Si el nodo raiz no tiene apuntador directo, entonces lo creamos
					crearBloquePunteros(contenedor);
					crearApuntadorDirecto2(ext2, raiz, contenedor);
				}

				if (index_raiz != -1) {
					leerBloque(ext2, contenedor, raiz->ap_directo2);
					dir = agregarPuntero(contenedor,
							ext2->superbloque->pinodol);
					if (dir == -1) {
						/*********** APUNTADOR INDIRECTO *************/
						if (raiz->ap_indirecto == -1) { //Si el nodo raiz no tiene apuntador indirecto, entonces lo creamos
							crearApuntadorIndirecto(ext2, raiz, contenedor);
						}

						if (index_raiz != -1) {
							leerBloque(ext2, contenedor, raiz->ap_indirecto);
							int pos = agregarPunteroPunteros(ext2, contenedor);
							if (pos != -1) {
								escribirBloque(ext2, contenedor,
										raiz->ap_indirecto);
								leerBloque(ext2, contenedor, pos);
								dir = agregarPuntero(contenedor,
										ext2->superbloque->pinodol);
								if (dir == -1) {
									printf(
											"La carpeta ya esta totalmente llena\n");
								} else {
									hijo->padre = index_raiz;
									escribirInodo(ext2, raiz, index_raiz);
									escribirBloque(ext2, contenedor, pos);
								}
							}
						}
					} else {
						hijo->padre = index_raiz;
						escribirInodo(ext2, raiz, index_raiz);
						escribirBloque(ext2, contenedor, raiz->ap_directo2);
					}
				}
			} else {
				hijo->padre = index_raiz;
				escribirInodo(ext2, raiz, index_raiz);
				escribirBloque(ext2, contenedor, raiz->ap_directo1);
			}
		}

		escribirInodo(ext2, hijo, ext2->superbloque->pinodol);

//------------- GUARDAMOS LOS CAMBIOS ---------------
		escribirBitmap(ext2);
		ext2->superbloque->pinodol = obtenerBitLibre(ext2->bitmap->inodos,
				NO_INODOS);
		escribirSuperBloque(ext2);
		{
			if (hijo != NULL )
				free(hijo);
			hijo = NULL;
		}
		{
			if (contenedor != NULL )
				free(contenedor);
			contenedor = NULL;
		}
		return dir;
	}
	return -1;
}

void imprimirSuperBloque(char *output, SuperBloque *sb) {
	FILE *reporte = fopen(output, "w");
	char *fcreacion = malloc(20 * sizeof(char));
	char *fmodificacion = malloc(20 * sizeof(char));
	char *fmount = malloc(20 * sizeof(char));

	getFecha(sb->fcreacion, fcreacion);
	getFecha(sb->fmodificacion, fmodificacion);
	getFecha(sb->fmount, fmount);

	fprintf(reporte,
			"No. Bloques: %d\nNo. Inodos: %d\nNo. Bloques Libres: %d\nNo. Inodos Libres: %d\nPrimer Bloque Libre: %d\nPrimer Inodo Libre: %d\nApuntador Primer Bloque: %d\nFecha Creacion: %s\nFecha Modificacion: %s\nFecha Montaje: %s\nPrimer Bit Libre Bloques: %d\nPrimer Bit Libre Inodos: %d\nEspacio Total en Bytes: %d\nEspacio Utilizado en Bytes: %d\n",
			sb->no_bloques, sb->no_inodos, sb->no_bloquesl, sb->no_inodosl,
			sb->pbloquel, sb->pinodol, sb->ap_pbloque, fcreacion, fmodificacion,
			fmount, sb->pbloquel, sb->pinodol, sb->no_bloques * 128,
			(sb->no_bloques - sb->no_bloquesl) * 128);

	free(fcreacion);
	free(fmodificacion);
	free(fmount);
	printf("Generando reporte de SuperBloque en %s...\n", output);
	fclose(reporte);
}

void imprimirBloque(FILE *reporte, Bloque *b) {
	int i;
	for (i = 0; i < sizeof(Bloque); i++) {
		if (reporte != NULL && *(b->contenido + i) != '\0')
			fputc(*(b->contenido + i), reporte);
	}
	if (reporte != NULL )
		fprintf(reporte, "\n");
}

void imprimirBloques(EXT2 *ext2, char *output) {
	Bloque *b = malloc(sizeof(Bloque));
	FILE *reporte = fopen(output, "w");
	int i;
	for (i = 0; i < NO_BLOQUES; i++) {
		leerBloque(ext2, b, i);
		fprintf(reporte, "Bloque %d :", i);
		imprimirBloque(reporte, b);
	}
	printf("Generando reporte de bloques en %s...\n", output);
	free(b);
	fclose(reporte);
}

void imprimirBitmap(char *output, Bitmap *bitmap) {
	FILE *reporte = fopen(output, "w");
	int i;
	printf("Generando reporte de bitmaps en %s...\n", output);
	fprintf(reporte, "----------------- BLOQUES -----------------\n");
	for (i = 0; i < NO_BLOQUES; i++) {
		fputc(*(bitmap->bloques + i), reporte);
	}
	fprintf(reporte, "\n----------------- INODOS -----------------\n");
	for (i = 0; i < NO_INODOS; i++) {
		fputc(*(bitmap->inodos + i), reporte);
	}
	fclose(reporte);
}

void crearBloqueTexto(Bloque *b) {
	int i;
	for (i = 0; i < sizeof(Bloque); i++) {
		*(b->contenido + i) = '\0';
	}
}

int ASCIIToInteger(char *cadena, int cifras) {
	int i, RESULT = 0, digit = 0;
	for (i = 0; i < cifras; i++) {
		digit = ((char) *(cadena + i)) - 48;
		RESULT = RESULT * 10 + digit;
	}
	return RESULT;
}

void IntegertoASCII(char *cadena, int entero, int cifras) {
	int i = 0;
	while (i < cifras) {
		*(cadena + cifras - i - 1) = (char) ((entero % 10) + 48);
		entero = entero / 10;
		i++;
	}
}

int buscarRuta(EXT2 *ext2, Inodo *raiz, char *id_inodo) {
	if (raiz->tipo != Dir) {
		printf("ERROR: '%s' no es un directorio...\n", raiz->id);
		return -1;
	}

	Inodo *aux = malloc(sizeof(Inodo));
	Bloque *puntero = malloc(sizeof(Bloque));
	int i;
	//----------- Buscamos en el apuntador directo ------------
	if (raiz->ap_directo1 != -1) {
		leerBloque(ext2, puntero, raiz->ap_directo1);
		for (i = 6; i < sizeof(Bloque); i += 6) {
			if (*(puntero->contenido + i) == ' ')
				continue;
			int p = ASCIIToInteger(puntero->contenido + i, 5);
			leerInodo(ext2, aux, p);
			if (compare(aux->id, id_inodo) == 0) {
				{
					if (puntero != NULL )
						free(puntero);
					puntero = NULL;
				}
				{
					if (aux != NULL )
						free(aux);
					aux = NULL;
				}
				return p;
			}
		}
	}

	if (raiz->ap_directo2 != -1) {
		leerBloque(ext2, puntero, raiz->ap_directo2);
		for (i = 6; i < sizeof(Bloque); i += 6) {
			if (*(puntero->contenido + i) == ' ')
				continue;
			int p = ASCIIToInteger(puntero->contenido + i, 5);
			leerInodo(ext2, aux, p);
			if (compare(aux->id, id_inodo) == 0) {
				{
					if (puntero != NULL )
						free(puntero);
					puntero = NULL;
				}
				{
					if (aux != NULL )
						free(aux);
					aux = NULL;
				}
				return p;
			}
		}
	}

	if (raiz->ap_indirecto != -1) {
		leerBloque(ext2, puntero, raiz->ap_indirecto);
		Bloque *ppuntero = malloc(sizeof(Bloque));
		int j;
		for (i = 6; i < sizeof(Bloque); i += 6) {
			if (*(puntero->contenido + i) == ' ')
				continue;
			int p = ASCIIToInteger(puntero->contenido + i, 5);
			leerBloque(ext2, ppuntero, p);
			for (j = 6; j < sizeof(Bloque); j += 6) {
				if (*(ppuntero->contenido + j) == ' ')
					continue;
				int p2 = ASCIIToInteger(ppuntero->contenido + j, 5);
				leerInodo(ext2, aux, p2);
				if (compare(aux->id, id_inodo) == 0) {
					{
						if (ppuntero != NULL )
							free(ppuntero);
						ppuntero = NULL;
					}
					{
						if (puntero != NULL )
							free(puntero);
						puntero = NULL;
					}
					{
						if (aux != NULL )
							free(aux);
						aux = NULL;
					}
					return p2;
				}
			}
		}
		{
			if (ppuntero != NULL )
				free(ppuntero);
			ppuntero = NULL;
		}
	}
	free(aux);
	free(puntero);
	return -1;
}

int _mkdir(EXT2 *ext2, char *id) {
	int i = 1;
	int size = len(id);

	int root = 0;
	Inodo *path = malloc(sizeof(Inodo));

	int inodo = 0;
	leerInodo(ext2, path, 0);

	char *hijo = malloc(15 * sizeof(char));

	while (1) {
		i = splitPath(i, id, hijo);
		if (hasText(hijo) == 0 || i > size + 1)
			break;

		inodo = buscarRuta(ext2, path, hijo);
		if (inodo == -1) {
			root = exec_mkdir(ext2, hijo, path, root, Dir);
			printf("Creando carpeta: %s en %s\n", hijo, path->id);
			char *fecha = malloc(20 * sizeof(fecha));
			time_t *h_actual = malloc(sizeof(time_t));
			setFecha(h_actual);
			getFecha(*h_actual, fecha);
			setFecha(&ext2->superbloque->fmodificacion);
			fprintf(ext2->log, "%s:Directorio %s creado.\n", fecha, hijo);
			fflush(ext2->log);
			free(fecha);
			free(h_actual);
		} else
			root = inodo;

		leerInodo(ext2, path, root);
	}

	free(path);
	free(hijo);
	return root;
}

int _mkfile(EXT2 *ext2, char *id, char *contenido) {
	int i = 1;
	int size = len(id);

	int root = 0;
	Inodo *path = malloc(sizeof(Inodo));

	int inodo = 0;
	leerInodo(ext2, path, 0);

	char *hijo = malloc(100 * sizeof(char));

	while (1) {
		i = splitPath(i, id, hijo);
		if (hasText(hijo) == 0 || i > size + 1)
			break;

		inodo = buscarRuta(ext2, path, hijo);
		if (inodo == -1) {
			root = exec_mkdir(ext2, hijo, path, root, Dir);
			printf("Creando %s en %s\n", hijo, path->id);
			char *fecha = malloc(20 * sizeof(fecha));
			time_t *h_actual = malloc(sizeof(time_t));
			setFecha(h_actual);
			getFecha(*h_actual, fecha);
			setFecha(&ext2->superbloque->fmodificacion);
			fprintf(ext2->log, "%s:%s creado.\n", fecha, hijo);
			fflush(ext2->log);
			free(fecha);
			free(h_actual);
		} else
			root = inodo;

		leerInodo(ext2, path, root);
	}

	path->tipo = File;

	exec_mkfile(ext2, contenido, path, root);
	escribirInodo(ext2, path, root);
	free(hijo);
	return root;
}

void _opfile(EXT2 *ext2, char *id) {
	int i = 1;
	int size = len(id);

	int root = 0;
	Inodo *path = malloc(sizeof(Inodo));

	int inodo = 0;
	leerInodo(ext2, path, 0);

	char *hijo = malloc(15 * sizeof(char));

	while (1) {
		i = splitPath(i, id, hijo);
		if (hasText(hijo) == 0 || i > size + 1)
			break;

		inodo = buscarRuta(ext2, path, hijo);
		if (inodo == -1) {
			printf("ERROR** %s no existe en %s\n", hijo, path->id);
			return;
		} else
			root = inodo;

		leerInodo(ext2, path, root);
	}
	if (path->tipo == File) {

		printf(
				"-------------------------------- %s -------------------------------\n",
				id);
		exec_opfile(ext2, path, root);
		char *fecha = malloc(20 * sizeof(fecha));
		time_t *h_actual = malloc(sizeof(time_t));
		setFecha(h_actual);
		getFecha(*h_actual, fecha);
		fprintf(ext2->log, "%s:Archivo %s abierto.\n", fecha, id);
		fflush(ext2->log);
		free(fecha);
		free(h_actual);
	} else
		printf("ERROR** %s no es un archivo\n", path->id);

	free(path);
	free(hijo);
}

int agregarTexto(Bloque *b, char *contenido) {
	int i = 0;
	for (i = 0; i < sizeof(Bloque) && *(contenido + i) != '\0'; i++) {
		*(b->contenido + i) = *(contenido + i);
	}
	return i - sizeof(Bloque);
}

int exec_mkfile(EXT2 *ext2, char *contenido, Inodo *raiz, int index_raiz) {
	if (raiz->tipo == File) { //Hay un inodo disponible
		Bloque *contenedor = malloc(sizeof(Bloque));

		int try = 0;
		int fsize = len(contenido);

		if (fsize > 128 + 128 + 128 * 20) {
			printf("El archivo es muy grande!\n");
			return -1;
		}

		raiz->fsize = fsize;

		if (fsize > 0 && raiz->ap_directo1 == -1) { //Si el nodo raiz no tiene apuntador directo, entonces lo creamos
			crearBloqueTexto(contenedor);
			crearApuntadorDirecto1(ext2, raiz, contenedor);
		}

		if (fsize > 0) {
			leerBloque(ext2, contenedor, raiz->ap_directo1);
			try = agregarTexto(contenedor, contenido);

			escribirInodo(ext2, raiz, index_raiz);
			escribirBloque(ext2, contenedor, raiz->ap_directo1);

			if (try >= 0 && fsize > sizeof(Bloque)) {
				if (raiz->ap_directo2 == -1) { //Si el nodo raiz no tiene apuntador directo, entonces lo creamos
					crearBloqueTexto(contenedor);
					crearApuntadorDirecto2(ext2, raiz, contenedor);
				}

				if (index_raiz != -1) {
					leerBloque(ext2, contenedor, raiz->ap_directo2);
					try = agregarTexto(contenedor, contenido + sizeof(Bloque));
					escribirInodo(ext2, raiz, index_raiz);
					escribirBloque(ext2, contenedor, raiz->ap_directo2);
					if (try >= 0 && fsize > sizeof(Bloque) * 2) {
						/*********** APUNTADOR INDIRECTO *************/
						if (raiz->ap_indirecto == -1) { //Si el nodo raiz no tiene apuntador indirecto, entonces lo creamos
							crearApuntadorIndirecto(ext2, raiz, contenedor);
						}

						if (index_raiz != -1) {
							int i = 2, pos;
							while (try == 0) {
								leerBloque(ext2, contenedor,
										raiz->ap_indirecto);
								pos = agregarPunteroPunteros(ext2, contenedor);
								if (pos != -1) {
									escribirBloque(ext2, contenedor,
											raiz->ap_indirecto);
									leerBloque(ext2, contenedor, pos);
									crearBloqueTexto(contenedor);
									try = agregarTexto(contenedor,
											contenido + sizeof(Bloque) * i);
									escribirInodo(ext2, raiz, index_raiz);
									escribirBloque(ext2, contenedor, pos);
									if (try == 0)
										i++;

								} else
									break;
							}
						}
					} else { //Limpiar los punteros
						*(ext2->bitmap->bloques + raiz->ap_indirecto) = '0'; //Indicamos que vamos a DESocupar ese bloque
						raiz->ap_indirecto = -1;
						ext2->superbloque->pbloquel = obtenerBitLibre(
								ext2->bitmap->bloques, NO_BLOQUES);
					}
				}
			} else { //Limpiar los punteros
				*(ext2->bitmap->bloques + raiz->ap_directo2) = '0'; //Indicamos que vamos a DESocupar ese bloque
				*(ext2->bitmap->bloques + raiz->ap_indirecto) = '0'; //Indicamos que vamos a DESocupar ese bloque
				raiz->ap_directo2 = -1;
				raiz->ap_indirecto = -1;
				ext2->superbloque->pbloquel = obtenerBitLibre(
						ext2->bitmap->bloques, NO_BLOQUES);
			}
		} else { //Limpiar los punteros
			*(ext2->bitmap->bloques + raiz->ap_directo1) = '0'; //Indicamos que vamos a DESocupar ese bloque
			*(ext2->bitmap->bloques + raiz->ap_directo2) = '0'; //Indicamos que vamos a DESocupar ese bloque
			*(ext2->bitmap->bloques + raiz->ap_indirecto) = '0'; //Indicamos que vamos a DESocupar ese bloque
			raiz->ap_directo1 = -1;
			raiz->ap_directo2 = -1;
			raiz->ap_indirecto = -1;
			ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
					NO_BLOQUES);
		}

//------------- GUARDAMOS LOS CAMBIOS ---------------
		escribirBitmap(ext2);
		ext2->superbloque->pinodol = obtenerBitLibre(ext2->bitmap->inodos,
				NO_INODOS);
		escribirSuperBloque(ext2);
		{
			if (contenedor != NULL )
				free(contenedor);
			contenedor = NULL;
		}

		return try;
	}
	return -1;
}

void exec_opfile(EXT2 *ext2, Inodo *raiz, int index_raiz) {
	Bloque *puntero = malloc(sizeof(Bloque));
	int i, handler = 0;
	//----------- Buscamos en el apuntador directo ------------
	if (raiz->ap_directo1 != -1) {
		leerBloque(ext2, puntero, raiz->ap_directo1);
		for (i = 0; i < sizeof(Bloque) && handler < raiz->fsize; i++) {
			printf("%c", *(puntero->contenido + i));
			handler++;
		}
	}

	if (raiz->ap_directo2 != -1) {
		leerBloque(ext2, puntero, raiz->ap_directo2);
		for (i = 0; i < sizeof(Bloque) && handler < raiz->fsize; i++) {
			printf("%c", *(puntero->contenido + i));
			handler++;
		}
	}

	if (raiz->ap_indirecto != -1) {
		leerBloque(ext2, puntero, raiz->ap_indirecto);
		Bloque *ppuntero = malloc(sizeof(Bloque));
		int j;
		for (i = 6; i < sizeof(Bloque) && handler < raiz->fsize; i += 6) {
			if (*(puntero->contenido + i) == ' ')
				continue;
			int p = ASCIIToInteger(puntero->contenido + i, 5);
			leerBloque(ext2, ppuntero, p);
			for (j = 0; j < sizeof(Bloque); j++) {
				printf("%c", *(ppuntero->contenido + j));
				handler++;
			}
		}
		{
			if (ppuntero != NULL )
				free(ppuntero);
			ppuntero = NULL;
		}
	}
	free(puntero);
	printf("\n");
}

int _rmRuta(EXT2 *ext2, char *id, int truncate) {
	int i = 1;
	int size = len(id);

	int root = 0;
	Inodo *path = malloc(sizeof(Inodo));

	int inodo = 0;
	leerInodo(ext2, path, 0);

	char *hijo = malloc(15 * sizeof(char));

	while (1) {
		i = splitPath(i, id, hijo);
		if (hasText(hijo) == 0 || i > size + 1)
			break;

		inodo = buscarRuta(ext2, path, hijo);
		if (inodo == -1) {
			printf("ERROR** %s no existe en %s\n", hijo, path->id);
			return -1;
		} else
			root = inodo;

		leerInodo(ext2, path, root);
	}

	_rmInodoRefs(ext2, path, root);
	escribirInodo(ext2, path, root);

	int padre = path->padre;

	if (truncate > 0) {
		int type = path->tipo;
		leerInodo(ext2, path, path->padre);
		//------------------- con estos datos procedemos a borrar
		Bloque *puntero = malloc(sizeof(Bloque));
		//----------- Buscamos en el apuntador directo ------------
		if (path->ap_directo1 != -1) {
			leerBloque(ext2, puntero, path->ap_directo1);
			if (borrarPuntero(puntero, root) == -1 && path->ap_directo2 != -1) {
				leerBloque(ext2, puntero, path->ap_directo2);
				if (borrarPuntero(puntero, root) == -1
						&& path->ap_indirecto != -1) {
					leerBloque(ext2, puntero, path->ap_indirecto);
					Bloque *ppuntero = malloc(sizeof(Bloque));
					for (i = 6; i < sizeof(Bloque); i += 6) {
						if (*(puntero->contenido + i) == ' ')
							continue;
						int p = ASCIIToInteger(puntero->contenido + i, 5);
						leerBloque(ext2, ppuntero, p);
						if (borrarPuntero(ppuntero, root) != -1) {
							escribirBloque(ext2, ppuntero, p);
							escribirBloque(ext2, puntero, path->ap_indirecto);
							*(ext2->bitmap->bloques + root) = '0';
							printf("%s %s eliminado\n",
									type == Dir ? "Directorio" : "Archivo", id);
							char *fecha = malloc(20 * sizeof(fecha));
							time_t *h_actual = malloc(sizeof(time_t));
							setFecha(h_actual);
							getFecha(*h_actual, fecha);
							setFecha(&ext2->superbloque->fmodificacion);
							fprintf(ext2->log, "%s:%s %s eliminado.\n", fecha,
									type == Dir ? "Directorio" : "Archivo", id);
							fflush(ext2->log);
							free(fecha);
							free(h_actual);
						}
					}
					if (ASCIIToInteger(puntero->contenido, 5) == 0) {
						*(ext2->bitmap->bloques + path->ap_indirecto) = '0';
						path->ap_indirecto = -1;
					} else {
						if (ppuntero != NULL )
							free(ppuntero);
						ppuntero = NULL;
						escribirBloque(ext2, puntero, path->ap_indirecto);
					}
				} else {
					if (ASCIIToInteger(puntero->contenido, 5) == 0) {
						*(ext2->bitmap->bloques + path->ap_directo2) = '0';
						path->ap_directo2 = -1;
					} else
						escribirBloque(ext2, puntero, path->ap_directo2);
					printf("%s %s eliminado\n",
							type == Dir ? "Directorio" : "Archivo", id);
					char *fecha = malloc(20 * sizeof(fecha));
					time_t *h_actual = malloc(sizeof(time_t));
					setFecha(h_actual);
					getFecha(*h_actual, fecha);
					setFecha(&ext2->superbloque->fmodificacion);
					fprintf(ext2->log, "%s:%s %s eliminado.\n", fecha,
							type == Dir ? "Directorio" : "Archivo", id);
					fflush(ext2->log);
					free(fecha);
					free(h_actual);
				}
			} else {
				if (ASCIIToInteger(puntero->contenido, 5) == 0) {
					*(ext2->bitmap->bloques + path->ap_directo1) = '0';
					path->ap_directo1 = -1;
				} else
					escribirBloque(ext2, puntero, path->ap_directo1);

				printf("%s %s eliminado\n",
						type == Dir ? "Directorio" : "Archivo", id);
				char *fecha = malloc(20 * sizeof(fecha));
				time_t *h_actual = malloc(sizeof(time_t));
				setFecha(h_actual);
				getFecha(*h_actual, fecha);
				setFecha(&ext2->superbloque->fmodificacion);
				fprintf(ext2->log, "%s:%s %s eliminado.\n", fecha,
						type == Dir ? "Directorio" : "Archivo", id);
				fflush(ext2->log);
				free(fecha);
				free(h_actual);
			}
		}

		escribirInodo(ext2, path, padre);
		{
			if (puntero != NULL )
				free(puntero);
			puntero = NULL;
		}
	}
	free(path);
	free(hijo);

	ext2->superbloque->pbloquel = obtenerBitLibre(ext2->bitmap->bloques,
			NO_BLOQUES);
	ext2->superbloque->pinodol = obtenerBitLibre(ext2->bitmap->inodos,
			NO_INODOS);
	escribirSuperBloque(ext2);
	escribirBitmap(ext2);
	return 0;
}

void _rmInodoRefs(EXT2 *ext2, Inodo *target, int index) {
	Bloque *contenedor = malloc(sizeof(Bloque));
	if (target->tipo == File) {
		//----------- Buscamos en el apuntador directo ------------
		if (target->ap_directo1 != -1) {
			*(ext2->bitmap->bloques + target->ap_directo1) = '0';
			target->ap_directo1 = -1;
		}
		if (target->ap_directo2 != -1) {
			*(ext2->bitmap->bloques + target->ap_directo2) = '0';
			target->ap_directo2 = -1;
		}
		if (target->ap_indirecto != -1) {
			*(ext2->bitmap->bloques + target->ap_indirecto) = '0';
			leerBloque(ext2, contenedor, target->ap_indirecto);
			int i;
			for (i = 6; i < sizeof(Bloque); i += 6) {
				if (*(contenedor->contenido + i) == ' ')
					continue;
				int puntero = ASCIIToInteger(contenedor->contenido + i, 5);
				*(ext2->bitmap->bloques + puntero) = '0';
			}
			target->ap_indirecto = -1;
		}

		target->fsize = 0;
	} else if (target->tipo == Dir) {
		Inodo *ino = malloc(sizeof(Inodo));
		//----------- Buscamos en el apuntador directo ------------
		if (target->ap_directo1 != -1) {
			*(ext2->bitmap->bloques + target->ap_directo1) = '0';
			leerBloque(ext2, contenedor, target->ap_directo1);
			int i;
			for (i = 6; i < sizeof(Bloque); i += 6) {
				if (*(contenedor->contenido + i) == ' ')
					continue;
				int puntero = ASCIIToInteger(contenedor->contenido + i, 5);
				leerInodo(ext2, ino, puntero);
				_rmInodoRefs(ext2, ino, puntero);
			}
			target->ap_directo1 = -1;
		}
		if (target->ap_directo2 != -1) {
			*(ext2->bitmap->bloques + target->ap_directo2) = '0';
			leerBloque(ext2, contenedor, target->ap_directo2);
			int i;
			for (i = 6; i < sizeof(Bloque); i += 6) {
				if (*(contenedor->contenido + i) == ' ')
					continue;
				int puntero = ASCIIToInteger(contenedor->contenido + i, 5);
				leerInodo(ext2, ino, puntero);
				_rmInodoRefs(ext2, ino, puntero);
			}
			target->ap_directo2 = -1;
		}
		if (target->ap_indirecto != -1) {
			*(ext2->bitmap->bloques + target->ap_indirecto) = '0';
			leerBloque(ext2, contenedor, target->ap_indirecto);
			Bloque *contenedor2 = malloc(sizeof(Bloque));
			int i, j;
			for (i = 6; i < sizeof(Bloque); i += 6) {
				if (*(contenedor->contenido + i) == ' ')
					continue;
				int puntero = ASCIIToInteger(contenedor->contenido + i, 5);
				leerBloque(ext2, contenedor2, puntero);
				for (j = 6; j < sizeof(Bloque); j += 6) {
					if (*(contenedor2->contenido + j) == ' ')
						continue;
					int puntero2 = ASCIIToInteger(contenedor2->contenido + j,
							5);
					leerInodo(ext2, ino, puntero2);
					_rmInodoRefs(ext2, ino, puntero2);
				}
			}
			{
				if (contenedor2 != NULL )
					free(contenedor2);
				contenedor2 = NULL;
			}
			target->ap_indirecto = -1;
		}

		{
			if (ino != NULL )
				free(ino);
			ino = NULL;
		}
	}
	*(ext2->bitmap->inodos + index) = '0';

	free(contenedor);
	return;
}
