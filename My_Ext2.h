/*
 * My_Ext2.h
 *
 *  Created on: 18/06/2013
 *      Author: julian
 */

#ifndef MY_EXT2_H_
#define MY_EXT2_H_
#include "stdio.h"
#include "time.h"
#include "stdlib.h"
/*	CALCULO INODOS
 *  int X = 10000000 - sizeof(SuperBloque);
 int Y = ((int) (0.29 * X)) / sizeof(Inodo)
 + ((int) (0.71 * X)) / sizeof(Bloque);
 int B = (X - (sizeof(Inodo) + 1)*Y) / (sizeof(Bloque) - sizeof(Inodo));
 int I = Y - B;
 printf("%d %d %d, %d", X, Y, B, I);
 */
#define NO_BLOQUES 60000
#define NO_INODOS 61016
#define MAX_APUNTADORES (sizeof(char)*(128/sizeof(int)))-1
#define BLOQUE 0
#define INODO 1
#define NULO 2
#define File 3
#define Dir 4

typedef struct {
	int no_inodos;
	int no_bloques;
	int no_inodosl;
	int no_bloquesl;
	int pbloquel;
	int pinodol;
	time_t fmount;
	time_t fmodificacion;
	time_t fcreacion;
	int ap_pbloque;
} SuperBloque;

typedef struct {
	int fsize;				//4bytes
	int no_bloques_asig;	//4bytes
	int ap_directo1;		//4bytes
	int ap_directo2;		//4bytes
	int ap_indirecto;		//4bytes
	char id[15];
	int padre;
	int tipo;
} Inodo;

typedef struct {
	char contenido[128];
} Bloque;

typedef struct {
	char bloques[NO_BLOQUES];
	char inodos[NO_INODOS];
} Bitmap;

typedef struct {
	FILE *disco, *log;
	SuperBloque *superbloque;
	Bitmap *bitmap;
	char *ruta;
} EXT2;

//----------- FUNCIONES DE CREACION/USO -----------
int crearEXT2(char*);
EXT2 *montarEXT2(char*);
void desmontarEXT2(EXT2*);
void crearMapabits(char*, int);
void getFecha(time_t, char *);
void setFecha(time_t*);
//----------- FUNCIONES DE I/O -------------
void escribirSuperBloque(EXT2*);
void escribirBitmap(EXT2*);
void escribirBloque(EXT2*, Bloque*, int);
void leerBloque(EXT2*, Bloque*, int);
void escribirInodo(EXT2*, Inodo*, int);
void leerInodo(EXT2*, Inodo*, int);

//----------- FUNCIONES DE MANEJO -----------
int obtenerBitLibre(char*, int);
int agregarPuntero(Bloque*, int);
int agregarTexto(Bloque*, char*);
void crearBloqueTexto(Bloque*);
int agregarPunteroPunteros(EXT2*, Bloque*);
void crearApuntadorDirecto1(EXT2*, Inodo*, Bloque*);
void crearApuntadorDirecto2(EXT2*, Inodo*, Bloque*);
void crearApuntadorIndirecto(EXT2*, Inodo*, Bloque*);
void crearBloquePunteros(Bloque*);
int exec_mkdir(EXT2*, char*, Inodo*, int, int);
int exec_mkfile(EXT2*, char*, Inodo*, int);
void exec_opfile(EXT2*, Inodo*, int);
int buscarRuta(EXT2*, Inodo*, char*);
int _mkdir(EXT2*, char*);
int _mkfile(EXT2*, char*, char*);
void _opfile(EXT2*, char*);
void _rm(EXT2*, char*);
int _rmRuta(EXT2*, char*, int);
void _rmInodoRefs(EXT2*, Inodo*, int);

//----------- FUNCIONES DE REPORTE -----------
void imprimirSuperBloque(char*,SuperBloque*);
void imprimirBloque(FILE*,Bloque*);
void imprimirBloques(EXT2*,char*);
void imprimirSuperBloques(char *,Bloque*);
void imprimirBitmap(char*, Bitmap *);
int ASCIIToInteger(char*, int);
void IntegertoASCII(char*, int, int);

#endif /* MY_EXT2_H_ */
