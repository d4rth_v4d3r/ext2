/*
 * String.c
 *
 *  Created on: 15/06/2013
 *      Author: julian
 */
#include "String.h"

int len(char *str) {
	int i = 0;
	while (*(str + i) != '\0')
		i++;
	return i;
}

int compare(char *str1, char *str2) {
	int size = len(str1);

	if (size != len(str2))
		return -1;

	int i;
	for (i = 0; i < size; i++)
		if (*(str1 + i) != *(str2 + i))
			return -1;
	return 0;
}

void copy(char *str1, char *str2) {
	int size = len(str1);

	int i;
	for (i = 0; i < size; i++)
		*(str1 + i) = '\0';
	size = len(str2);
	for (i = 0; i < size; i++)
		*(str1 + i) = *(str2 + i);
}

void reset(char *str, int size) {
	int i;
	for (i = 0; i < size; i++)
		*(str + i) = '\0';
}

int hasText(char *str) {
	int i;
	for (i = 0; i < 15; i++)
		if (*(str + i) != '\0')
			return 1;
	return 0;
}

void append(char *str, int size1, char *toAppend) {
	int size2 = len(toAppend);
	int i, j = 0;
	for (i = size1; i < size1 + size2; i++) {
		*(str + i) = *(toAppend + j);
		j++;
	}

}

int splitPath(int index, char *absolutePath, char *splitted) {
	int i;
	int pointer = 0;

	for (i = 0; i < 15; i++)
		*(splitted + i) = '\0';

	i = index;
	while (*(absolutePath + i) != '/' && *(absolutePath + i) != '\0') {
		*(splitted + pointer) = *(absolutePath + i);
		++pointer;
		++i;
	}
	return i + 1;
}

