/*
 * String.h
 *
 *  Created on: 15/06/2013
 *      Author: julian
 */

#ifndef STRING_H_
#define STRING_H_

int compare(char*, char*);
int len(char*);
void reset(char*, int);
void copy(char*, char*);
void append(char*, int, char*);
int splitPath(int, char*, char*);
int hasText(char*);

#endif /* STRING_H_ */
